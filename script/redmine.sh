#!/bin/bash

# Pré-requisitos

DATABASE_HOST=$1

if [[ ! "$DATABASE_HOST"  ]]
        then
        echo " Parametros nao encontrados."
        echo " ./redmine.sh <DATABASE_HOST> "
        exit -1
fi

# Atualizacao

sudo yum update -y

## Instalação do Redmine 2.3

# Instalando EPEL

sudo yum install -y wget
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

sudo yum clean all
# 1. Instale os requisitos

sudo yum -y install zlib-devel curl-devel openssl-devel httpd-devel apr-devel apr-util-devel subversion git postgresql-devel gcc gcc-c++.x86_64 make automake autoconf curl-devel openssl-devel zlib-devel httpd-devel apr-devel apr-util-devel sqlite-devel libxslt-devel libxml2-devel.x86_64 php-pear ImageMagick ImageMagick-devel ImageMagick-perl vim

# 2. Adicionando /usr/local/bin no path

sudo sed -i 's/\/sbin:\/bin:\/usr\/sbin:\/usr\/bin/\/sbin:\/bin:\/usr\/sbin:\/usr\/bin:\/usr\/local\/bin/' /etc/sudoers

# 3. Compilando o ruby

mkdir /tmp/ruby && cd /tmp/ruby
sudo curl --progress http://ftp.ruby-lang.org/pub/ruby/2.0/ruby-2.0.0-p451.tar.gz | tar xz
cd ruby-2.0.0-p451
./configure --disable-install-rdoc
make
sudo make prefix=/usr/local install

# 4. Instalando bundle

sudo gem install bundle --no-ri --no-rdoc

# 5. Instalando o redmine 2.3-stable

cd /opt
sudo svn co http://svn.redmine.org/redmine/branches/2.3-stable redmine

# 6. Instalando gems

sudo chown -R $USER:$GROUP redmine
cd /opt/redmine
bundle install --without mysql sqlite
sudo gem install unicorn --no-ri --no-rdoc
sudo gem install pg -v '0.17.1' --no-ri --no-rdoc

# 7. Configurar database.yml no Redmine#

cd config/
mv database.yml.example database.yml
echo "production:
  adapter: postgresql
  database: redmine
  host: $1
  username: redmine
  password: redmine
  encoding: utf8" > database.yml

# 8. Populando Redmine

rake generate_secret_token
RAILS_ENV=production rake db:migrate
echo "pt-BR" | RAILS_ENV=production rake redmine:load_default_data

# 9. Configurando Unicorn

cd /opt/redmine
mkdir pids
wget https://gitlab.com/softwarepublico/colabdocumentation/raw/master/Arquivos/redmine/unicorn.rb -O config/unicorn.rb

# 10. Configurando routes.rb

wget https://gitlab.com/softwarepublico/colabdocumentation/raw/master/Arquivos/redmine/routes.rb -O config/routes.rb

# 11. Adicionando link simbólico

ln -s /opt/redmine/public /opt/redmine/public/redmine

# 12. Instalando o plugin do backlogs

cd /opt/redmine/plugins
git clone https://github.com/backlogs/redmine_backlogs.git
cd redmine_backlogs
git checkout v1.0.6

# 13. Bundle install

RAILS_ENV=production
export RAILS_ENV
bundle install

# 14. Corrigindo dependências

sudo gem uninstall rack -v '1.5.2'

# 15. Bundle install redmine

cd /opt/redmine
bundle install --without mysql sqlite
bundle exec rake db:migrate
bundle exec rake redmine:backlogs:install story_trackers=2 task_tracker=1

#16. Instalando plugin do Remote User
cd /opt/redmine/plugins
git clone https://github.com/colab-community/single_auth.git

# 17. Executando redmine

sudo /usr/local/bin/unicorn_rails -c /opt/redmine/config/unicorn.rb -E production -p 80 -D
