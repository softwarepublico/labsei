
#!/bin/sh

##---------------------------------------
#       Variaveis Utilizadas 
##----------------------------------------
DIR_BACKUP="/var/backup/colab-sei"
LOCALHOST="localhost"
DATABASE_REDMINE="redmine"
PORT_POSTGRES=5432
REDMINE_USER="redmine"
PASSWORD_REDMINE_USER="redmine"
REDMINE_DIRECTORY="/opt/redmine"

#make dirs

sudo mkdir -p $DIR_BACKUP

#---------------------------------------


COMMAND=$1

if [ "$COMMAND" == "backup" ];then
	echo "BACKUP"
#	#backup ldap
	sudo slapcat -v -l $DIR_BACKUP/ldap-backup.ldif

	#backup redmine postgres
	export PGPASSWORD=$PASSWORD_REDMINE_USER
	/usr/pgsql-9.3/bin/pg_dump -i -h $LOCALHOST -p $PORT_POSTGRES -U $REDMINE_USER -Fc -b -v -f $DIR_BACKUP/redmine.dump  $DATABASE_REDMINE	

	#backup redmine files	
	sudo rsync -bvza $REDMINE_DIRECTORY/files $DIR_BACKUP/files_backup --delete

	exit 0
fi

if [ "$COMMAND" == "restore"  ] ; then
	echo "RESTORE"

	#restore ldap
	
	#remove old users
	sudo service slapd stop
	sudo rm /var/lib/ldap/alock /var/lib/ldap/log* /var/lib/ldap/*db* -f
	sudo service slapd start
	sudo service slapd stop

       #adicionar os usuários antigos
	sudo slapadd -l  $DIR_BACKUP/ldap-backup.ldif
	sudo slapindex -v
	sudo chown ldap:ldap /var/lib/ldap/*
	service slapd start


	# reset database redmine

	PID_REDMINE_UNICORN=`cat $REDMINE_DIRECTORY/pids/unicorn.pid `
	sudo kill -9 $PID_REDMINE_UNICORN

	sudo -u postgres psql -c "DROP DATABASE $DATABASE_REDMINE;"
        sudo -u postgres psql -c "CREATE DATABASE $DATABASE_REDMINE WITH ENCODING='UTF8' OWNER=$REDMINE_USER;"


       # restore database redmine
	export PGPASSWORD=$PASSWORD_REDMINE_USER
       	/usr/pgsql-9.3/bin/pg_restore --host=$LOCALHOST --port=$PORT --dbname=$DATABASE_REDMINE --username=$REDMINE_USER    $DIR_BACKUP/redmine.dump -n public -v

	sudo rsync -bvza $DIR_BACKUP/files_backup/files $REDMINE_DIRECTORY  --delete 

	sudo /usr/local/bin/unicorn_rails -c /opt/redmine/config/unicorn.rb -E production -p 80 -D

fi
