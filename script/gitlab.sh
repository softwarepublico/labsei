#!/bin/bash

# Coloque aqui os ips do gitlab e do redmine
IP_GITLAB=
IP_REDMINE=
IP_LDAP=
PASSWORD_LDAP=

while [ -e $IP_GITLAB ]
do
  echo "Digite o IP externo do Gitlab: "
  read IP_GITLAB
  echo "IP digitado: $IP_GITLAB" 
  echo "Confirma (y/n)?"
  read CONFIRMA
  if [ "$CONFIRMA" != "y" ] ; then
    IP_GITLAB=
  fi
done

while [ -e $IP_REDMINE ]
do      
  echo "Digite o IP externo do Redmine: "
  read IP_REDMINE
  echo "IP digitado: $IP_REDMINE" 
  echo "Confirma (y/n)?"
  read CONFIRMA
  if [ "$CONFIRMA" != "y" ] ; then
    IP_REDMINE=
  fi
done

while [ -e $IP_LDAP ]
do      
  echo "Digite o IP externo do LDAP: "
  read IP_LDAP
	echo "Senha Ldap:"
	read PASSWORD_LDAP
  echo "IP digitado: $IP_LDAP" 
	echo "PASSWORD digitado foi: $PASSWORD_LDAP"
  echo "Confirma (y/n)?"
  read CONFIRMA
  if [ "$CONFIRMA" != "y" ] ; then
    IP_LDAP=
    PASSWORD_LDAP=
  fi
done

# Atualizar o sistema
sudo yum update -y

#1. Instalar dependencias

sudo yum install wget vim postfix openssh-server -y

#2. Fazer o download do gitlab

sudo wget https://downloads-packages.s3.amazonaws.com/centos-6.5/gitlab-7.0.0_omnibus-1.el6.x86_64.rpm

#3. Instale o Gitlab

echo "Instalando gitlab, leva alguns minutos"
sudo rpm -i gitlab-7.0.0_omnibus-1.el6.x86_64.rpm

#4. Criar o arquivo de configuracao do gitlab

echo "Criando arquivo de configuracao do gitlab"
sudo mkdir -p /etc/gitlab
sudo touch /etc/gitlab/gitlab.rb
sudo chmod 600 /etc/gitlab/gitlab.rb

#5. Adicionar configuracoes para o gitlab

echo "Editando as configuracoes do gitlab"
sudo echo "redis['port'] = 1234" >> /etc/gitlab/gitlab.rb
sudo echo "postgresql['port'] = 2345" >> /etc/gitlab/gitlab.rb
sudo echo "unicorn['port'] = 3456" >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['internal_api_url'] = 'http://localhost:3456'" >> /etc/gitlab/gitlab.rb
sudo echo "external_url \"http://$IP_GITLAB\"" >> /etc/gitlab/gitlab.rb

#6. Adicionar integracao com redmine

sudo echo "gitlab_rails['issues_tracker_redmine'] = 'redmine'" >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['issues_tracker_redmine_title'] = 'Redmine'" >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['issues_tracker_redmine_project_url'] = 'http://$IP_REDMINE/redmine/projects/:issues_tracker_id'" >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['issues_tracker_redmine_issues_url'] = 'http://$IP_REDMINE/redmine/issues/:id'" >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['issues_tracker_redmine_new_issue_url'] = 'http://$IP_REDMINE/redmine/projects/:issues_tracker_id/issues/new'" >> /etc/gitlab/gitlab.rb

#7. Liberando firewall para servisos http e ssh
echo "Adicionando regras no firewall para habilitar http e ssh, caso estejam desabilitados"
#sudo lokkit -s http -s ssh

# Liberando porta 22, isso e' essencial para o funcionamento do git
# Caso queira restringir o acesso para redes especificas, favor adicionar opcao
# -s 192.168.0.0/24
#sudo iptables -A INPUT -p tcp --dport 22 -j ACCEPT

echo "configurando ldap"

localhost=$IP_LDAP
base="dc=sei,dc=com"
port="389"
bind_dn="cn=admin,$base"
password=$PASSWORD_LDAP

sudo echo "gitlab_rails['ldap_enabled'] = true" >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_host'] = '$localhost' " >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_port'] = $port"  >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_base'] = '$base'"  >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_uid'] = 'uid'"  >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_method'] = 'plain'"  >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_bind_dn'] = '$bind_dn'"  >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_password'] = '$password'"  >> /etc/gitlab/gitlab.rb
sudo echo "gitlab_rails['ldap_allow_username_or_email_login'] = true"  >> /etc/gitlab/gitlab.rb

echo "Iniciando gitlab"
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart

#8. Mostrando os dados do gitlab

echo "Sucesso! Para acessar o gitlab, abra no navegador o ip $IP_GITLAB"
echo "Usuario: root"
echo "Senha: 5iveL!fe"
